import { useEffect, useState } from 'react'
import BoardCell from './BoardCell'
import { BoardRow, H2, P } from '../styledComponents'
import { BoardConfig, Cell, GameBoard } from '../types'
import { createBoard, getCellIdsToReveal, revealBoard } from '../utils'

const Board = ({boardConfig}: {boardConfig: BoardConfig}): JSX.Element => {

  const [remainingMines, setRemainingMines] = useState(boardConfig.mines)
  const [gameStatus, setGameStatus] = useState<string>('Game started')
  const [gameBoard, setGameBoard] = useState<GameBoard>(createBoard({boardConfig}))

  useEffect(()=>{
    setGameBoard(createBoard({boardConfig}))  
  },[boardConfig])

  const handleLeftClick = (checkCell: Cell) => {
    if (!checkCell.isRevealed && !checkCell.isFlagged) {
      if (checkCell.isMine) {
        setGameStatus('You')
        setGameBoard(revealBoard(gameBoard))
        return
      }

      const cellsToReveal = getCellIdsToReveal({alreadyProcessed: {}, checkCell, gameBoard})
      const updatedBoard = gameBoard.map(row =>
        row.map(cell => {
          if (cellsToReveal?.[cell.id])
            return {...cell, isFlagged: false, isRevealed: true}
          return cell
        })
      )
      const hiddenMines: number[] = []
      updatedBoard.forEach(row=> row.forEach(cell => !cell.isFlagged && cell.isMine && hiddenMines.push(cell.id)))
      if (hiddenMines.length === 0) {
        setGameStatus('Je hebt gewonnen')
      }

      setGameBoard(updatedBoard)
    }
  }
  const handleRightClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, cell: Cell) => {

    event.preventDefault()
    if (cell.isRevealed) return

    const updatedCell = {...cell, isFlagged: !cell.isFlagged}
    setRemainingMines(updatedCell.isFlagged ? remainingMines-1 : remainingMines+1)

    const updatedGameBoard = [...gameBoard]
    updatedGameBoard[cell.row][cell.col] = updatedCell

    const hiddenMines: number[] = []
    updatedGameBoard.forEach(row=> row.forEach(cell => !cell.isFlagged && cell.isMine && hiddenMines.push(cell.id)))
    if (hiddenMines.length === 0) {
      setGameStatus('Je hebt gewonnen')
      setGameBoard(revealBoard(updatedGameBoard))
    } else {
      setGameBoard(updatedGameBoard)
    }
  }

  return (
    <>
      <H2>Remaining mines: {remainingMines}</H2>
      <P>{gameStatus}</P>
      {gameBoard.map((row, index) => (
        <BoardRow key={index}>
          {row.map(cell =>
            <BoardCell key={cell.id}
              value={cell}
              onLeftClick={() => handleLeftClick(cell)}
              onRightClick={(event) => handleRightClick(event, cell)}
            />
          )}
        </BoardRow>
      )
      )}
    </>
  )
}

export default Board
