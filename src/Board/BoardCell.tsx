import { MouseEventHandler } from 'react'
import { StyledCell } from '../styledComponents'
import { Cell } from '../types'

const getValue = (value: Cell) => {

  if (value.isFlagged) return '🚩'
  if (!value.isRevealed) return null

  if (value.isMine) return '💣'
  return value.neighbour
}

interface Props {
    value: Cell,
    onLeftClick: MouseEventHandler<HTMLDivElement>,
    onRightClick: MouseEventHandler<HTMLDivElement>
}

const BoardCell = ({value, onLeftClick, onRightClick}: Props): JSX.Element => 
  <StyledCell value={value} onClick={onLeftClick} onContextMenu={onRightClick}>
    {getValue(value)}
  </StyledCell>

export default BoardCell
