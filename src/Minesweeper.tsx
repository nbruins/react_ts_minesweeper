import { ChangeEvent, useState } from 'react'
import { BoardConfig, Level, Levels } from './types'
import { BoardContainer, Fieldset, H1 } from './styledComponents'
import Board from './Board/Board'


const levels: Levels = {
  beginner: {size: 8, mines: 10},
  intermediate: {size: 12, mines: 20},
  expert: {size: 16, mines: 40},
}
const Minesweeper = (): JSX.Element => {

  const [selectedLevel, setSelectedLevel] = useState<Level>('beginner')
  const [boardConfig, setBoardConfig] = useState<BoardConfig>(levels[selectedLevel])

  const handleSelect = (event: ChangeEvent<HTMLSelectElement>) => {

    const level = (event.target.value as Level)
    setSelectedLevel(level)
    setBoardConfig(levels[level])
  }

  return (
    <BoardContainer>
      <H1>
        React Minesweeper
      </H1>
      <Fieldset>
        <label htmlFor="select_level">Level</label>
        <select id="select_level" value={selectedLevel} onChange={handleSelect}>
          {Object.keys(levels).map((level, index) => <option key={index} value={level}>{level}</option>)}
        </select>
      </Fieldset>
      <Board boardConfig={boardConfig}/>
    </BoardContainer>
  )
}

export default Minesweeper
