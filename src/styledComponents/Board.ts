import styled from '@emotion/styled'
import { Cell } from '../types'

const colors = {
  background: '#054A29',
  color: '#FFF',
  revealed: '#FF865E',
  hidden: '#14B86B'
}

export const BoardContainer = styled.div(() => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center'
}))


export const BoardRow = styled.div(() => ({
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'nowrap',
  justifyContent: 'center'
}))


export const StyledCell = styled.div<{ value: Cell }>((props) => ({
  background: props.value.isRevealed ? colors.background : colors.hidden,
  border: `1px solid ${colors.color}`,
  float: 'left',
  lineHeight: '45px',
  height: '45px',
  textAlign: 'center',
  width: '45px',
  cursor: 'pointer',
  borderRadius: '5px',
  color: (props.value.isFlagged || props.value.isMine) ? colors.color : colors.color,
  fontWeight: 600,
}))
