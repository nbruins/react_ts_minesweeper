import styled from '@emotion/styled'

const textAlign = 'center'

export const H1 = styled.h1(() => ({
  margin: 0,
  fontWeight: 300,
  fontSize: '3.75rem',
  lineHeight: '1.2',
  letterSpacing: '-0.00833em',
  marginBottom: '0.35em',
  textAlign
}))

export const H2 = styled.h2(() => ({
  textAlign
}))

export const P = styled.p(() => ({
  textAlign
}))

export const Fieldset = styled('fieldset')`
  width: fit-content;
  align-self: center;
  padding: 8px 16px;
  margin: 10px;
  & > label {
    padding-right: 8px;
  }

`