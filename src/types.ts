export interface BoardConfig {
  size: number,
  mines: number
}

export interface Cell {
    id: number
    row: number
    col: number
    isMine: boolean
    neighbour: null | number
    isRevealed: boolean,
    isFlagged: boolean,
}

export type GameBoard = Array<Array<Cell>>

export type Level = 'beginner' | 'intermediate' | 'expert'

export type Levels = {
  [key in Level]: BoardConfig
}