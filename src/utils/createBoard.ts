import { BoardConfig, GameBoard } from '../types'
import { getNeigbouringCells, getRandomNumber } from '.'

const createEmptyBoard = (size: number): GameBoard => {

  let id = 0
  const board: GameBoard = Array.from(Array(size),
    (_, rowIndex) => {
      const row = Array.from(Array(size), (_, colIndex) => {
        const col = {
          id,
          row: rowIndex,
          col: colIndex,
          isMine: false,
          neighbour: 0,
          isRevealed: false,
          isFlagged: false,
        }
        id++
        return col
      })
      id++
      return row
    })
  return board
}

const plantMines = (emptyBoard: GameBoard, boardConfig: BoardConfig): GameBoard => {
  let minesPlanted = 0

  while (minesPlanted < boardConfig.mines) {
    const row = getRandomNumber(boardConfig.size)
    const col = getRandomNumber(boardConfig.size)
    if (emptyBoard?.[row]?.[col]?.isMine === false) {
      emptyBoard[row][col].isMine = true
      minesPlanted++
    }
  }

  return emptyBoard
}

const setNeighbours = (boardWithMines: GameBoard): GameBoard => {

  return boardWithMines.map(row =>
    row.map(cell => {
      if (cell.isMine) return cell
      const neighbouringCells = getNeigbouringCells(cell.row, cell.col, boardWithMines)
      let mine = 0
      neighbouringCells.forEach(value => { value.isMine && mine++})
      cell.neighbour = mine > 0 ? mine: null
      return cell
    })
  )
}

export const createBoard = ({boardConfig}: {boardConfig: BoardConfig}): GameBoard => {

  const emptyBoard = createEmptyBoard(boardConfig.size)
  const boardWithMines = plantMines(emptyBoard, boardConfig)
  return setNeighbours(boardWithMines)
}
