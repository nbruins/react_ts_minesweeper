import { getNeigbouringCells } from '.'
import { Cell, GameBoard } from '../types'

interface ProcessedCells {
    [id: number]: {reveal: boolean}
}

interface Props {
    alreadyProcessed: ProcessedCells
    checkCell: Cell
    gameBoard: GameBoard
}

export const getCellIdsToReveal = ({alreadyProcessed, checkCell, gameBoard}: Props): ProcessedCells => {

  let processedCells: ProcessedCells = {}
  if (alreadyProcessed?.[checkCell.id] === undefined) {
    processedCells = {...alreadyProcessed, [checkCell.id]: {reveal: false}}
    if (!checkCell.isFlagged && !checkCell.isRevealed)
      processedCells[checkCell.id] = {reveal: true}
    if (checkCell.neighbour === null) {
      const neighbouringCells = getNeigbouringCells(checkCell.row, checkCell.col, gameBoard)
      neighbouringCells.forEach(cell => {
        if (processedCells?.[cell.id] === undefined)
          processedCells = {...getCellIdsToReveal({alreadyProcessed: processedCells, checkCell: cell, gameBoard})}
      })
    }
  }
        
  return processedCells
}