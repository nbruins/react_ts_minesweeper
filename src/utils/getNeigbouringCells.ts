import { Cell, GameBoard } from '../types'

export const getNeigbouringCells = (row: number, col: number, gameBoard: GameBoard): Cell[] => {
  const neighbouringCells: Cell[] = []
  const rows = gameBoard.length ?? 0
  const cols = gameBoard[0].length ?? 0

  // row up
  if (row > 0 && col > 0) neighbouringCells.push(gameBoard[row - 1][col - 1])
  if (row > 0) neighbouringCells.push(gameBoard[row - 1][col])
  if (row > 0 && col < cols - 1) neighbouringCells.push(gameBoard[row - 1][col + 1])

  // same row
  if (col > 0) neighbouringCells.push(gameBoard[row][col - 1])
  if (col < cols - 1)neighbouringCells.push(gameBoard[row][col + 1])

  // row down
  if (row < rows - 1 && col < cols - 1) neighbouringCells.push(gameBoard[row + 1][col + 1])
  if (row < rows - 1) neighbouringCells.push(gameBoard[row + 1][col])
  if (row < rows - 1 && col > 0) neighbouringCells.push(gameBoard[row + 1][col - 1])

  return neighbouringCells
}