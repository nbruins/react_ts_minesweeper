import { GameBoard } from '../types'

export const revealBoard = (gameBoard: GameBoard): GameBoard =>
  gameBoard.map(row => row.map(cell => ({...cell, isFlagged: false, isRevealed: true})))