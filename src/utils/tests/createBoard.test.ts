import { createBoard } from '..'


describe('Is the board created correctly', () => {
  const size = 8
  const mines = 12

  it('Test number of rows, columns and mines', () => {

    const board = createBoard({boardConfig: {size, mines}})
    expect(board.length).toEqual(size)
    expect(board[0].length).toEqual(size)

    let minesOnBoard = 0
    board.forEach(row=> row.forEach(cell=> {
      cell.isMine && minesOnBoard++
    }))
    expect(minesOnBoard).toEqual(mines)

  })
})
