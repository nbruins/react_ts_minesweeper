import { getRandomNumber } from '..'

describe('Is the random number within the given range', () => {

  it('Test multiple times if random number is in range', () => {

    for (let i = 0; i < 1000; i++) {
      const randomNumber = getRandomNumber(10)
      expect(randomNumber).toBeLessThanOrEqual(10)
      expect(randomNumber).toBeGreaterThanOrEqual(0)
    }
  })
})
